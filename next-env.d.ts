/// <reference types="next" />
/// <reference types="next/types/global" />

declare namespace gaussian {
  interface Gaussian {
    random(x: number): number[];
  }
}

declare module 'gaussian' {
  function gaussian(mean: number, variance: number): gaussian.Gaussian;
  export default gaussian;
}

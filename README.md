# Galton Board

## Step by Step Walkthrough

The first time I opened up and read the challenge, I was actually very nervous. I don't have any idea what is a Galton Board. The explanations written in the Wikipedia actually made me even more nervous. As nervous as I was, I tried to fire up a search with the query `galton board` on Google, and found out this [site](https://galtonboard.com/). This finally cleared my mind of all question marks and I thought I would be able to solve it.

So, there was actually two problems that I could think of by the time I understand what a Galton Board is. The first one being how to simulate where the balls should fall; and the second one being how to design the web app itself, as I'm not the best UI designer at all.

### Simulating the board

As the result of a run of the Galton Board itself resembles the shape of the Gaussian Distribution bell curve, simulating it isn't a very big problem. This is what's inside my mind.

1. Generate a list (array) of random number using the Gaussian distribution. These numbers indicate the position where each of the balls are about to fall. To make things easier to imagine, I decided to start using the Gaussian distribution with `mean = 5` and `variance = 1`. The landing position of the board, then can be obtained by rounding down the generated random numbers. Here's a few example.

| Generated random number | Bucket number (index of the array) |
|-------------------------|------------------------------------|
| 6.6                     | 6                                  |
| 3.5                     | 3                                  |
| 0.3                     | 0                                  |
| -9.0                    | 0                                  |
| 18.3                    | 9                                  |

2. When one of the bucket is clicked, the Galton Board is going to run again, with a fewer balls (from the clicked bucket), and with a different starting position, causing the `mean` value of the bell curve to shift according to the position of the clicked bucket. This is how I'm visualizing it in my mind.

![](./docs/gaussian.png)

With the solutions in mind, the next is to configure the workspace and project directory.

### Project Setup

There are lots of things in my mind that I want to configure for this challenge. I'll talk through it one by one.

#### Next.js

As this challenge is all about React and Next.js, I decided to come up with Next.js as it's easier to bootstrap. We don't need an external library to handle the routing. I create the next project by using their CLI `create-next-app`. I run the following command.

```bash
yarn create next-app
```

The CLI will then prompt for a project name and the project directory will be bootstrapped in a neat fashion. I could also run the following command to bootstrap my next app.

```bash
yarn init
yarn add next react react-dom
```

I chose to use the CLI as it's easier and I don't have to worry about typos in my folders.

#### Tailwind CSS

Tailwind CSS is a PostCSS plugin, so to utilize it, we also need to install PostCSS. Hence, I run the following command to install the dependencies.

```bash
yarn add tailwindcss postcss autoprefixer -D
```

The `-D` adds the packages as a dev-dependencies. Then, I run the following command to generate the default `tailwind.config.js` and `postcss.config.js`.

```bash
yarn run tailwindcss init -p
```

Then, I added the purge path to tree-shake the unused styles in production.

PS: As I was referring to the documentation, I later ended spending 30 minutes to figure out why my production build doesn't have any CSS bundled. I was apparently using the path `/pages` and `/components` in my `tailwind.config.js`; meanwhile, I later moved my source codes into the `src` directory. It's a silly mistake, that I hopefully won't bump into in the future. (I have actually done this same mistake in the past)

Next, I'm including Tailwind in my CSS by editing my `globals.css`.

```css
@tailwind base;
@tailwind component;
@tailwind utilities;
```

The `globals.css` is already imported, so Tailwind is now up and running.

#### ESLint and TypeScript

I like to have a linter installed to help prevent silly errors and mistakes that could happen sometimes. Type checks are also important to help us do some statical analysis before our code goes into production. As ESLint and TypeScript tends to have conflicting and redundant rules, I love to setup both of them at the same time. Here's how I installed the required packages.

```bash
yarn add typescript eslint eslint-plugin-react @typescript-eslint/eslint-plugin @typescript-eslint/parser @types/react @types/node -D
```

Then, I initialized my `.eslintrc.json` using the CLI.

```bash
yarn run eslint --init
```

Next, I created my `tsconfig.js` to configure TypeScript. When we run `yarn dev` (`next dev`), the `tsconfig.js` will actually be created or updated to the recommended values by the next CLI.

By this step, I also decided to add some npm scripts to make it easier to do linting and type checks. I also write a doctor script that is supposed to run before commit.

#### Prettier

I love code formatters; and Prettier is by far my favorite code formatters out there (it's also integrated directly to my text editor). So, here's what I do to install prettier.

```bash
yarn add prettier eslint-config-prettier -D
```

The eslint config here is needed as prettier tends to have some rules that result in a conflict with eslint recommended rules. I also setup my `.prettierrc.json` to my likings.

Next, I wrote two npm scripts to check the project's code format and to run the formatter. Prettier is integrated to my text editor, so I actually don't really need those scripts.

#### Jest and Testing Library

As I love to test my own codes, I need to install the testing-related tools to actually go and test my codes. My favorite suite of tools for React application is Jest + Testing Library. I love Testing Library as the APIs make testing components similar to end-to-end testing. Here's how I installed those dependencies.

```bash
yarn add jest jest-dom babel-jest @types/jest @testing-library/dom @testing-library/jest-dom @testing-library/react -D
```

Then, I initialized the `jest.config.js` using jest CLI.

```bash
yarn run jest --init
```

Next, I would like to extend my jest assertion to extend from testing library. So, I created a new file called `setupTest.ts` with the following code and updated `jest.config.js` to use this file as the setup script.

```typescript
import '@testing-library/jest-dom/extend-expect';
```

I also added an npm script to run jest in watch mode using the `--watchAll` flag.

#### Husky

Husky is pretty useful as it provides cool git hooks that allows us to run some script each time those git hooks are triggered. I'm going to utilize the `pre-commit` hook to run my `doctor` script and check if the source code passes the test and comply to the linter, formatter and typescript rules.

```bash
yarn add husky -D && yarn run husky init
```

Then, I create a pre-commit hook using the husky CLI.

```bash
yarn run husky add .husky/pre-commit 'npx jest --passWithNoTests && npm run doctor'
```

And it's all done. The project is now configured.

### Coding the App (without any styling)

I've just recently learnt TDD (Test-Driven Development) and fell in love with it. The way our code are driven by the test itself makes our code invulnerable to regressions (bug that emerges from refactors). Also, the way we work on with each step of small iterations make the code tidier and nicer.

#### Rendering the Favicon From the Document

`create-next-app` provided us with a basic template. However, the template itself renders the favicon via home page. As I plan to have the same favicon for my whole web app, I decided to create a custom Document component that includes the favicon. That's why I have my `/pages/_document.tsx` file.

#### First Iteration: Rendering a Title that Says "Andrew's Galton Board"

Then, I started writing some tests for the home page to enter the red phase of TDD. The code tested if the home page is rendered with a correct title that says `Andrew's Galton Board` with an `<h1>` tag. Entering the red phase, I then wrote the React code to pass the tests, and refactor. However, I didn't have anything to refactor by the time.

#### Second Iteration: Rendering The Buckets

Next, I write tests that tested the home page component whether it renders the buckets along with the numbers (indicating the number of balls in a bucket) correctly. And, again, I am on the red phase. I then wrote the code to pass the tests and refactor. Here, I create a separate component called bucket. Prior to that, I again wrote the test codes to satisfy the usage of the bucket component. By the end of this bigger iteration, my application is now rendering the buckets correctly.

#### Third Iteration: Simulating the Galton Board

Same as the previous iterations, I first wrote the test codes, this time testing that the home page simulates the Galton Board. However, it is hard to write the tests if I put the code on the home page. This is an indication that I should refactor early before even writing the tests. So, I decided to write a module called galton. This module has only one function that is important for the home page. In short, it simulates the Galton Board using the steps I explained in the beginning of this documentation. Of course, I wrote this module in a TDD way; write the test, fail the tests, write the code, pass the tests, refactor. Finally, I was able to write the tests for the home page by mocking the galton module. Then, it goes the same as before. And now the app is simulating a galton board run when it first loads.

To make things easier, I installed an npm package named `gaussian` to help me generate the gaussian distribution with ease.

#### Fourth Iteration: Run Another Process of Galton Board When One of the Bucket is Clicked

This iteration seems to be very complicated, however, this is actually the simplest part of the Galton Board, provided that I have spent my time thinking about it (at the beginning of this documentation). So, I wrote the tests, as usual, implement the idea which I explained at the beginning of this documentation. The galton module was also mocked to make the tests predictable (as the galton module is dealing with generating random numbers). And now, the app was working smoothly, simulating each of the Galton Board run.

#### Fifth Iteration: Render a Histogram (Bar Chart)

Finally, I need to show a histogram indicating the number of balls in each bucket on the latest row of buckets. To make things easier, I decided to use `chart.js`, a library to render chart with ease. I've been using this library on some of my past projects, so the APIs look familiar to me. However, charts are hard to test. So, I opted to mock the chart component and just test the data related instead. Then, I write the code to pass the tests, again; and refactor. Now, I have a fully-functional app tailored according to the challenge statements. The last step is to style the app to make it more appealing.

### Deploying on Netlify

Netlify supports deployment of static site. As this web app doesn't render on the server, I decided to make use of Netlify for deployment. Everything seems to be working fine. I linked my repository to my Netlify account, added the build scripts `next build && next export`, and set the output directory as `/out`. However, after the deployment succeeds, I do find out that the Tailwind CSS is not bundled, but the test codes are included in the bundle. I spent almost 2 hours to solve this problem. Apparently, the test codes gets bundled because I put the `__tests__` folder inside the 'src' directory. As with the Tailwind, it is actually bundled, but everything is tree-shaken and left out as I provide the wrong path at `tailwind.config.css`. Once I fixed those stuffs, the web is up and running smoothly.

## NPM Scripts

| Scripts      | What it does                                 |
|--------------|----------------------------------------------|
| dev          | run next app in development mode             |
| build        | build the app                                |
| start        | start the production server                  |
| test         | run jest in watch mode                       |
| lint         | run linter checks                            |
| format       | format the code according to prettier config |
| format-check | check the project code format                |
| type-check   | run typescript type checks                   |
| doctor       | verify project is ready to be committed      |

## Limitations

The web app itself should only look decent on a device with a large screen, as I didn't focus on styling for mobile and tablet versions.

import { render, fireEvent, waitFor } from '@testing-library/react';
import HomePage from '../../src/pages/index';
import galton from '../../src/helpers/galton';

jest.mock('../../src/helpers/galton');
jest.mock('react-chartjs-2');
const mockedGalton = galton as jest.Mocked<typeof galton>;
mockedGalton.simulate
  .mockReturnValueOnce([1, 5, 8, 0, 0])
  .mockReturnValueOnce([1, 5, 8, 0, 0])
  .mockReturnValueOnce([1, 5, 8, 0, 0])
  .mockReturnValueOnce([1, 5, 8, 0, 0])
  .mockReturnValue([0, 0, 0, 0, 0]);

describe('HomePage', () => {
  it('renders the header correctly', () => {
    const { getByText } = render(<HomePage />);
    const element = getByText("Andrew's Galton Board");
    expect(element.tagName).toBe('H1');
  });

  it('renders a galton board with 11 buckets, 1 on top, 10 on the bottom', () => {
    const { getAllByTestId } = render(<HomePage />);
    const elements = getAllByTestId('galton-bucket');
    expect(elements).toHaveLength(11);
  });

  it('simulates a run of galton board on first-load', () => {
    const { getAllByTestId } = render(<HomePage />);
    const elements = getAllByTestId('galton-bucket');

    expect(mockedGalton.simulate).toHaveBeenCalledTimes(1);
    expect(mockedGalton.simulate).toHaveBeenCalledWith(10000, 5);
    waitFor(() => {
      expect(elements[1]).toHaveTextContent('2');
      expect(elements[2]).toHaveTextContent('1');
      expect(elements[6]).toHaveTextContent('1');
      expect(elements[9]).toHaveTextContent('1');
    });
  });

  it('simulates another run of galton board when clicked', () => {
    const { getAllByTestId } = render(<HomePage />);
    const elements = getAllByTestId('galton-bucket');

    fireEvent.click(elements[6]);
    waitFor(() => {
      expect(elements[1]).toHaveTextContent('5');
    });
  });

  it('reset the galton board when the reset button is clicked', () => {
    const { getByText } = render(<HomePage />);
    const resetButton = getByText('Reset');

    fireEvent.click(resetButton);
    waitFor(() => {
      expect(mockedGalton.simulate).toHaveBeenCalledTimes(2);
      expect(mockedGalton.simulate).toHaveBeenCalledWith(10000, 5);
    });
  });
});

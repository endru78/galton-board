import galton, { getBucketNumber } from '../../src/helpers/galton';
import gaussian from 'gaussian';

jest.mock('gaussian');
const mockedGaussian = gaussian as jest.Mocked<any>;

describe('galton', () => {
  describe('getBucketNumber()', () => {
    it('returns the value, rounded-down', () => {
      expect(getBucketNumber(5.6)).toBe(5);
      expect(getBucketNumber(0.3)).toBe(0);
      expect(getBucketNumber(9.6)).toBe(9);
    });

    it('return 9 if input exceeded 10', () => {
      expect(getBucketNumber(100.3)).toBe(9);
    });

    it('return 0 if input is smaller than 0', () => {
      expect(getBucketNumber(-7.3)).toBe(0);
    });
  });

  describe('.simulate()', () => {
    mockedGaussian.mockReturnValue({
      random(): number[] {
        return [2.2, 5.6, 100.9, 2.6, 3.3];
      },
    });
    it('returns ball counts in each bucket in the form of an array', () => {
      const expected = [2, 5, 9, 2, 3];
      expect(galton.simulate(5, 5)).toEqual(expected);
    });
  });
});

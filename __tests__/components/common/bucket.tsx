import { render, fireEvent } from '@testing-library/react';
import Bucket from '../../../src/components/common/bucket';

describe('Bucket', () => {
  it('render 0 ball count when no props provided', () => {
    const { getByTestId } = render(<Bucket />);
    const element = getByTestId('galton-bucket');
    expect(element).toHaveTextContent('0');
  });

  it('renders the ball count correctly', () => {
    const { getByTestId } = render(<Bucket ballCount={10} />);
    const element = getByTestId('galton-bucket');
    expect(element).toHaveTextContent('10');
  });

  it('calls onClick callback when clicked', () => {
    const onClick = jest.fn();
    const { getByTestId } = render(<Bucket onClick={onClick} />);
    const element = getByTestId('galton-bucket');
    fireEvent.click(element);
    expect(onClick).toHaveBeenCalledTimes(1);
  });
});

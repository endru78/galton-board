import React from 'react';

interface BucketProps {
  ballCount?: number;
  onClick?: () => void;
}

const Bucket: React.VFC<BucketProps> = ({ ballCount, onClick }) => {
  return (
    <div data-testid="galton-bucket" className="bucket" onClick={onClick}>
      {ballCount}
    </div>
  );
};

Bucket.defaultProps = {
  ballCount: 0,
};

export default Bucket;

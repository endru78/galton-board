import React from 'react';

const Container: React.FC<Record<string, unknown>> = props => {
  return (
    <div className="bg-gradient-to-b from-mango w-full row justify-center p-4">
      <div className="w-full max-w-4xl flex flex-col items-center" {...props} />
    </div>
  );
};

export default Container;

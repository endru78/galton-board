import gaussian from 'gaussian';

/**
 * Translates a generated random number into bucket index (bucket position)
 *
 * > Note: the bucket position / index can be obtained simply by rounding down
 * > the random number
 *
 * @param {number} x - the random number
 */
export const getBucketNumber = (x: number): number => {
  if (x > 10) return 9;
  if (x < 0) return 0;
  return Math.floor(x);
};

export default {
  /**
   * Simulates a galton board run provided the number of balls and mean. Should
   * return an array that indicates the landing position of each ball
   *
   * > Note: it is assumed that there will only be 10 buckets for the sake of
   * > simplicity in this challenge
   *
   * @param {number} n - number of balls
   * @param {number} mean -  mean
   */
  simulate(n: number, mean: number): number[] {
    const distribution = gaussian(mean, 1);
    const pool = distribution.random(n);
    return pool.map(getBucketNumber);
  },
};

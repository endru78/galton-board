import { useEffect, useState } from 'react';
import Head from 'next/head';
import { Bar } from 'react-chartjs-2';
import Bucket from '../components/common/bucket';
import Container from '../components/layouts/container';
import chartConstants from '../constants/chart.constants';
import galton from '../helpers/galton';

export default function Home(): JSX.Element {
  const [buckets, setBuckets] = useState(Array.from({ length: 10 }, () => 0));
  const [parentBucketCount, setParentBucketCount] = useState(10000);
  const [running, setRunning] = useState(true);

  const simulateGalton = (ballCount: number, startingPoint: number): void => {
    setBuckets(Array.from({ length: 10 }, () => 0));
    setRunning(true);
    setParentBucketCount(ballCount);

    const landingPositions = galton.simulate(ballCount, startingPoint);

    landingPositions.forEach(landingPosition => {
      setTimeout(() => {
        setBuckets(prevState => [
          ...prevState.slice(0, landingPosition),
          prevState[landingPosition] + 1,
          ...prevState.slice(landingPosition + 1),
        ]);
        setParentBucketCount(prevState => prevState - 1);
      }, 1);
    });
  };

  useEffect(() => {
    simulateGalton(10000, 5);
  }, []);

  useEffect(() => {
    if (parentBucketCount === 0) {
      setRunning(false);
    }
  }, [parentBucketCount]);

  return (
    <Container>
      <Head>
        <title>Andrew&apos;s Galton Board</title>
      </Head>

      <h1 className="mb-4">Andrew&apos;s Galton Board</h1>
      <Bucket ballCount={parentBucketCount} />
      <div className="row space-between my-8">
        {buckets.map((ballCount, i) => (
          <Bucket
            key={`#galton-${i}`}
            onClick={() => !running && simulateGalton(ballCount, i + 0.5)}
            ballCount={ballCount}
          />
        ))}
      </div>

      {running ? (
        <h3 className="mt-8">The Galton Board is running ...</h3>
      ) : (
        <Bar
          data={{
            labels: Array.from({ length: 10 }, (_, i) => `Bucket-${i + 1}`),
            datasets: [
              {
                label: 'Number of balls',
                data: buckets,
                backgroundColor: chartConstants.backgroundColor,
                borderColor: chartConstants.borderColor,
                borderWidth: 1,
              },
            ],
          }}
          redraw
        />
      )}

      <button
        className="fixed bottom-16 right-16 bg-red-500 text-white"
        onClick={() => simulateGalton(10000, 5)}
      >
        Reset
      </button>
    </Container>
  );
}
